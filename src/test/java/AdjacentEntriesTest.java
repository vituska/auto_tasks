import org.junit.Test;

import static org.junit.Assert.*;

public class AdjacentEntriesTest {

    @Test
    public void testGetSequenceOfNonAdjNumbers() {
        int[] testArray = AdjacentEntries.getSequenceOfNonAdjNumbers(8);
        validateArray(testArray);
    }

    @Test
    public void negativeTestGetSequenceOfNonAdjNumbers() {
        validateArray(new int[]{1,2,3,4});
    }

    private static void validateArray(int[] array){
        for (int i = 1; i < array.length - 1; i++){
            assertNotEquals(array[i], array[i+1]-1);
            assertNotEquals(array[i], array[i+1]+1);

            assertNotEquals(array[i], array[i-1]-1);
            assertNotEquals(array[i], array[i-1]+1);
        }
    }
}