import org.junit.Test;

import static org.junit.Assert.*;

public class TwoDimensionalArrayTest {

    @Test
    public void testRotate() {
        int n = 3;
        int[][] matrix = {
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };
        int[][] rotatedMatrix = TwoDimensionalArray.rotate(n, matrix);
        validateMatrix(rotatedMatrix);
    }

    private static void validateMatrix(int[][] rotatedMatrix){
        int[][] expectedMatrix = {{7,4,1}, {8,5,2}, {9,6,3}};

        assertArrayEquals(rotatedMatrix, expectedMatrix);
    }
}