import org.junit.Test;

import static org.junit.Assert.*;

public class NumberOfMovesTest {

    @Test
    public void testCountOfMovements() {
        int n = 13;
        int actualNumberOfMoves = NumberOfMoves.countNumberOfMovements(n);
        assertEquals(5, actualNumberOfMoves);
    }
}