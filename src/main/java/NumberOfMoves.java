public class NumberOfMoves {

    public static void main(String[] args) {
        int n = 35;
        int numberOfMoves = countNumberOfMovements(n);
        System.out.println("Number of moves is: " + numberOfMoves);
    }

    static int countNumberOfMovements(int n){
        int movesCounter = 0;

        while (n != 1){
            if(n%2 == 0){
                movesCounter++;
                n = n/2;
            } else {
                n--;
                movesCounter++;
            }
        }
        return movesCounter;
    }
}
