public class AdjacentEntries {

    public static void main(String[] args) {
        int n = 8;
        display(getSequenceOfNonAdjNumbers(n));
    }

    static int[] getSequenceOfNonAdjNumbers(int n){
        assert n > 3;
        int[] array = new int[n];
        int j = 0;

        for (int i = 2; i <=n; i+=2){
            array[j++] = i;
        }
        for(int i = 1; i <= n; i+=2){
            array[j++] = i;
        }
        return array;
    }

    static void display(int[] array){
        for(int i : array){
            System.out.print(" " + i);
        }
    }


}
