public class TwoDimensionalArray {

    public static void main(String[] args) {
        int n = 3;
        int[][] matrix = {
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };
        display(n, matrix);

        display(n, rotate(n, matrix));
    }

    static void display(int n, int[][] matrix){
        for(int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                System.out.print(" " + matrix[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    static int[][] rotate(int n, int[][] matrix){
        int[][] temp = new int[n][n];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                temp[i][j] = matrix[n - j - 1][i];
            }
        }
        return temp;
    }
}
